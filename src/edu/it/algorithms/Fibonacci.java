package edu.it.algorithms;

public class Fibonacci
{
//0 1 1 2 3 5 8 13 21 34 55 89
  public static void main(String[] args) {
    for(int i = 0; i <= 10; i++ )
    {
      int resultado = fibonacci(i);
      System.out.println(resultado);
    }
    
  }
  
  
  static int  fibonacci(int n)
  {
      if (n>1){
         return fibonacci(n-1) + fibonacci(n-2);  //funci�n recursiva
      }
      else if (n==1) {  // caso base
          return 1;
      }
      else if (n==0){  // caso base
          return 0;
      }
      else{ //error
          System.out.println("Debes ingresar un tama�o mayor o igual a 1");
          return -1; 
      }
  }
}
